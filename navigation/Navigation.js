
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack' 
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';


import HomeScreen from '../components/Tabs/HomeScreen'
import ListChatScreen from '../components/Tabs/ListChatScreen'
import ProfileScreen from '../components/Tabs/ProfileScreen'
import RootStack from './RootStack'
import { View } from 'react-native-animatable';
const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

class Navigation extends React.Component{
createBottomTab=()=>{
    return(
    <Tab.Navigator>
    <Tab.Screen name="Home" component={HomeScreen} />
    <Tab.Screen name="List Chat" component={ListChatScreen} />
    <Tab.Screen name="Profile" component={ProfileScreen} /> 
    </Tab.Navigator>
    )}
        render(){   
        return (
            <NavigationContainer >
                <RootStack/>
               
                {/* <Stack.Navigator screenOptions={{
                    headerStyle:{backgroundColor:'#009387'},
                    headerTintColor:'#fff',
                    headerTitleStyle:{fontWeight:'bold'}}}>
                    <Stack.Screen name="Login" component={Login} />
                    <Stack.Screen name="Signup" component={Signup} />
                    <Stack.Screen name="Bottom Tabs" children={this.createBottomTab} />
                </Stack.Navigator> */}
            </NavigationContainer>
       
            );
            }
}
export default Navigation