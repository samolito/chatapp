import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack' 
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Login from '../components/Screens/Login'
import Signup from '../components/Screens/Register'
import SplashScreen from '../components/Screens/SplashScreen';
import HomeScreen from '../components/Screens/HomeScreen';
import ChatScreen from '../components/Screens/ChatScreen';
const Tab = createBottomTabNavigator();

const RootStack = createStackNavigator();
createBottomTab=()=>{
    return(
    <Tab.Navigator>
    <Tab.Screen name="Home" component={HomeScreen} />
    <Tab.Screen name="List Chat" component={ListChatScreen} />
    <Tab.Screen name="Profile" component={ProfileScreen} /> 
    </Tab.Navigator>
    )}
const RootStackScreen=()=>{    
   
        return(
    <RootStack.Navigator headerMode='none'>
        <RootStack.Screen name="HomeScreen" component={HomeScreen}/>
        <RootStack.Screen name="SplashScreen" component={SplashScreen}/>
        <RootStack.Screen name="Login" component={Login}/>
        <RootStack.Screen name="Signup" component={Signup}/>
        
       <RootStack.Screen name="ChatScreen" component={ChatScreen}/>
       
        {/* <RootStack.Screen name="Bottom Tabs" children={this.createBottomTab}/> */}
    </RootStack.Navigator>
    )  
}
export default RootStackScreen 