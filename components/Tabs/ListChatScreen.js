/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import {StyleSheet, View, Text, Button } from 'react-native';


ListChatScreen=()=>
  <View style={styles.container}>
  
      <Text style={styles.welcome}>
    List Chat Screen
      </Text>
  <Button onPress={() => { alert('List Chat'); }}
    title="Details"/>
</View>


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

export default ListChatScreen;
