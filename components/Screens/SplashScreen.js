/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Dimensions
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as Animatable from 'react-native-animatable';
class SplashScreen extends React.Component {


 render(){

  return (
    <View style={styles.container}>
    <View style={styles.header}>
        <Animatable.Image
        animation="bounceIn"
        source={require('../images/log.png')}
         resizeMode="contain" 
        />
    </View>
    <Animatable.View style={styles.footer} animation="fadeInUpBig">
        <Text style={styles.title}>Stay connected with everyone</Text>
        <Text style={styles.text}>Sign in with account</Text>
        <View style={styles.button}>
          <TouchableOpacity onPress={()=>{this.props.navigation.navigate("Login")}} >
            <LinearGradient 
            colors={['#26ADFF','#75BEEA']} style={styles.signIn}>
              <Text style={styles.textSign}>Get Started</Text>
              <Icon name='navigate-next' color="white" size={20}></Icon>
            </LinearGradient>
          </TouchableOpacity>
        </View>
    </Animatable.View>
  </View>
  )}
};

  const {height}= Dimensions.get("screen");
  const {height_logo}= height * 0.28
  const styles = StyleSheet.create({
    container:{
      flex:1,
      backgroundColor:'#26ADFF',
   },
   logo:{
    width:height_logo,
    height:height_logo
 },
   header:{
    flex:2,
    justifyContent:'center',
    alignContent:'center',
    },
    footer:{
        flex:1,
        backgroundColor:'#fff',
        borderTopRightRadius:30,
        borderTopLeftRadius:30,
        paddingHorizontal:30,
        paddingVertical:50
    },
    
  title: {
    color:'#26ADFF',
    fontSize:30,
    fontWeight:'bold'
  },
  text: {
    color:'grey',
    marginTop:5
  },
  button: {
    alignItems:'flex-end',
    marginTop:30
  },
  signIn: {
    width:250,
    height:40,
    justifyContent:'center',
    alignItems:'center',
    borderRadius:50,
    flexDirection:'row'
  },
  textSign: {
    color:'white',
   fontWeight:'bold'
  },
});

export default SplashScreen;