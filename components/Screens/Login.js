/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment, useReducer} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TextInput,
  Image,
  Dimensions,
  Button
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient'
import { TouchableOpacity } from 'react-native-gesture-handler';

import auth from '@react-native-firebase/auth'
class Login extends React.Component  {
    constructor(props){
        super(props) 
        this.state = {
          email:'',
          password:'',
          errorMessage: null ,
         isLoading :false
        };
        
    }
    _displaySignup=()=>{
        //alert('id Film :'+idfilm)
        this.props.navigation.navigate("Signup", {signup:'The greatness'});
    }
    // _displayBottomtab=()=>{
    //     //alert('id Film :'+idfilm)
    //     this.props.navigation.navigate("Bottom Tabs");
    // }
    _onTextInputChanged = key => val=>{
      this.setState({ [key] : val })
    }
    _onSubmitLogin=()=>{
     if(this.state.email.length < 3){
        alert('email incorrect')
      }
      else{
      //firebase.database().ref('users').set({name:this.state.phone})
      //alert(this.state.email+"\n"+this.state.password)
      //this.props.navigation.navigate("Bottom Tabs");
       auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then(() => this.props.navigation.navigate('HomeScreen'))
      .catch(error => this.setState({ errorMessage: error.message }))
      alert(this.state.errorMessage)
       }
      
    }
    render(){
  return (
    <View style={styles.container}>
    <View style={styles.header}>
      <Text style={styles.text_header}>Welcome to ChatAPP</Text>
    </View>

    <View style={styles.footer}>
    <Text style={styles.text_footer}>Email</Text>
    <View style={styles.action}>
    <TextInput 
        style={styles.text_Input} 
        placeholder="Email" 
        autoCapitalize='none'
        keyboardType='email-address'
        value={this.state.email}
        onChangeText={this._onTextInputChanged('email')}
    /> 
  </View>

  <Text style={[styles.text_footer, {marginTop:30}]}>Password</Text>
  <View style={styles.action}>
    <TextInput 
        style={styles.text_Input}
        secureTextEntry={true}
        placeholder="your password" 
        autoCapitalize='none'
        value={this.state.password}
        onChangeText={this._onTextInputChanged('password')}
    /> 
  </View>
  
    <View style={styles.button}>
    <TouchableOpacity onPress={()=>{this._onSubmitLogin()}}>
     <LinearGradient
      colors={['#26ADFF','#75BEEA']}
      style={styles.signIn}>
        <Text style={[styles.textSign, {color:'#fff'}]}>Sign In</Text>
        </LinearGradient>
      </TouchableOpacity>

        <TouchableOpacity onPress={()=>{this._displaySignup() }}
                style={ {
                marginTop:30
               }}>
        <Text  style={ styles.text_footer}>Create a new account</Text>
        </TouchableOpacity>
        
      </View>
    </View>
  </View>
  )}
};


const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#26ADFF',
 },

 header:{
  flex:1,
  justifyContent:'flex-end',
  alignContent:'center',
  margin:20
  },
  footer:{
      flex:3,
      backgroundColor:'#fff',
      borderTopRightRadius:30,
      borderTopLeftRadius:30,
      paddingHorizontal:30,
      paddingVertical:50
  },
  action:{
    flexDirection:'row',
    marginTop:5,
    borderBottomWidth:1,
    borderBottomColor:'#f2f2f2',
    paddingBottom:5
  },
  title: {
    color:'#26ADFF',
    fontSize:30,
    fontWeight:'bold'
  },
  text_header: {
    color:'#fff',
    fontWeight:'bold',
    fontSize:40
  },
  text_footer: {
    color:'#3585B4',
    fontSize:18
  },
  text_Input: {
    flex:1,
    color:'gray',
    paddingLeft:10
  },
  button: {
    alignItems:'center',
    marginTop:30
  },
  signIn: {
    width:250,
    height:40,
    justifyContent:'center',
    alignItems:'center',
    borderRadius:50,
    flexDirection:'row'
  },
  textSign: {
    color:'white',
    fontWeight:'bold'
  },
  });

export default Login;
