
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TextInput
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient'
import { TouchableOpacity } from 'react-native-gesture-handler';
import database from '@react-native-firebase/database';
class Signup extends React.Component  {
    constructor(props){
        super(props) 
        this.state = {
          fullname:'',
          phone:'',
          email:'',
          password:'',
          confirmpass:'',
        isLoading :false
        };
        
    }
   
  _displayLogin=()=>{
      this.props.navigation.navigate("Login");
  }
  _onTextInputChanged = key => val=>{
    this.setState({ [key] : val })
  }
  _onCreateAccount=()=>{
    if(this.state.fullname.length<3){
      alert('Your name is incorrect')
    }
    else if(this.state.phone.length<10){
      alert('Phone incorrect')
    }
   if(this.state.password != this.state.confirmpass){
      alert('Your password must be same')
    }
    else{
      //try {
      //alert(this.state.fullname+"\n"+this.state.email+"\n"+this.state.phone+"\n"+this.state.password)
      database().ref('users/'+ this.state.phone).set({
          fullname:this.state.email, 
          phone:this.state.phone,
          password:this.state.password
        })  
        this.props.navigation.navigate("Login");
      // let resp = auth().createUserWithEmailAndPassword(this.state.email,this.state.password)
      }
  }
    render(){
  return (
    <View style={styles.container}>
    <View style={styles.header}>
      <Text style={styles.text_header}>Register now</Text>
    </View>
    
    <View style={styles.footer}>
    <ScrollView style={styles.scrollView}>
    <View style={styles.action}>
    <TextInput 
        style={styles.text_Input} 
        placeholder="Full Name" 
        autoCapitalize='none'
        value={this.state.fullname}
        onChangeText={this._onTextInputChanged('fullname')}
    /> 
  </View>
  <View style={styles.action}>
    <TextInput 
        style={styles.text_Input} 
        placeholder="Phone" 
        autoCapitalize='none'
        keyboardType='number-pad'
        value={this.state.phone}
        onChangeText={this._onTextInputChanged('phone')}
    /> 
  </View>
  <View style={styles.action}>
    <TextInput 
        style={styles.text_Input} 
        placeholder="email" 
        autoCapitalize='none'
        value={this.state.email}
        onChangeText={this._onTextInputChanged('email')}
    /> 
  </View>
  <View style={styles.action}>
    <TextInput 
        style={styles.text_Input}
        secureTextEntry={true}
        placeholder="your password" 
        autoCapitalize='none'
        value={this.state.password}
        onChangeText={this._onTextInputChanged('password')}
    /> 
  </View>
  <View style={styles.action}>

    <TextInput 
        style={styles.text_Input}
        secureTextEntry={true}
        placeholder="Confirm password" 
        autoCapitalize='none'
        value={this.state.confirmpass}
        onChangeText={this._onTextInputChanged('confirmpass')}
    /> 
  </View>
  
    <View style={styles.button}>
    <TouchableOpacity onPress={()=>{this._onCreateAccount()}}>
     <LinearGradient
      colors={['#26ADFF','#75BEEA']}
      style={styles.signIn}>
        <Text style={[styles.textSign, {color:'#fff'}]}>Sign Up</Text>
        </LinearGradient>
    </TouchableOpacity>
    
        <TouchableOpacity onPress={()=>{this._displayLogin() }}
                style={ {
                marginTop:30
               }}>
            <Text style={ styles.text_footer}>Go to login</Text>
        </TouchableOpacity>
       
        
      </View>
      </ScrollView>
    </View>
   
  </View>
  )}
};


const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#26ADFF',
 },

 header:{
  flex:1,
  justifyContent:'flex-end',
  alignContent:'center',
  margin:10
  },
  footer:{
      flex:4,
      backgroundColor:'#fff',
      borderTopRightRadius:30,
      borderTopLeftRadius:30,
      paddingHorizontal:30,
      paddingVertical:20
  },
 
  action:{
    flexDirection:'row',
    marginTop:5,
    borderBottomWidth:1,
    borderBottomColor:'#f2f2f2',
    paddingBottom:5
  },
  title: {
    color:'#26ADFF',
    fontSize:30,
    fontWeight:'bold'
  },
  text: {
    color:'grey',
    marginTop:5
  },
  text_header: {
    color:'#fff',
    fontWeight:'bold',
    fontSize:40
  },
  text_footer: {
    color:'#3585B4',
    fontSize:18
  },
  text_Input: {
    flex:1,
    color:'#414751',
    paddingLeft:10,
    fontSize:14
  },
  button: {
    alignItems:'center',
    marginTop:30
  },
  signIn: {
    width:250,
    height:40,
    justifyContent:'center',
    alignItems:'center',
    borderRadius:50,
    flexDirection:'row'
  },
  textSign: {
    color:'white',
    fontWeight:'bold'
  },
  });

export default Signup;
