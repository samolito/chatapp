import  React from 'react'
import {View, Text} from 'react-native'

import database from '@react-native-firebase/database';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import colors from '../theme/colors'
import { StyleSheet,Button } from 'react-native'
export default class HomeScreen extends React.Component{
    
    constructor(props){
        super(props) 
        this.state = {
          users:[],
        };
        
    }
    componentDidMount(){
        let dbRef = database().ref('users')
        dbRef.on('child_added',(val)=>{
            let person = val.val();
            person.phone = val.key;
            this.setState((prevState)=>{
                return{
                    users:[...prevState.users, person]
                }
            })
        })
    }
    rendeRow=({item})=>{
        return(
            <TouchableOpacity
            onPress={()=>{this.props.navigation.navigate('ChatScreen',item)}}>
                <Text>{item.fullname}</Text>
                <Text>{item.phone}</Text>

            </TouchableOpacity>
        )
    }
  render(){
      return(
          <SafeAreaView style={styles.container}>
              <FlatList
              data={this.state.users}
              keyExtractor={(item) =>item.fullname.toString()}
              renderItem={this.rendeRow}
              />
          </SafeAreaView>
      )
  }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 56
      },
    addButton: {
        padding: 12,
        borderRadius: 50,
        backgroundColor: colors.primary,
        position: 'absolute',
        bottom: 24,
        right: 24,
        elevation: 10
      }
  })