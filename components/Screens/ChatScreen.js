import  React from 'react'
import {View,TextInput, Text, FlatList} from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context';
import { TouchableOpacity } from 'react-native-gesture-handler';
import colors from '../theme/colors'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { StyleSheet } from 'react-native'
export default class ChatScreen extends React.Component{
    static navigationOptions=({navigation})=>{
        return{
            title:this.props.navigation.getParam('fullname')
        }
    }
    constructor(props){
        super(props) 
        this.state = {
          textMessage:'',
         isLoading :false
        };
        
    }
    _onTextInputChanged = key => val=>{
        this.setState({ [key] : val })
      }
    render(){
        return(
            <View style={{ flex: 1 }}>
            <FlatList
            style={{ flexGrow: 1 }}
            ></FlatList>
            <SafeAreaView style={[style.container]}>
                <TextInput 
                    style={style.textInput}
                    underlineColorAndroid={'transparent'}
                    placeholder="Type your message here" 
                    autoCapitalize='none'
                    value={this.state.textMessage}
                    onChangeText={this._onTextInputChanged('textMessage')}
                /> 
                <TouchableOpacity
                style={style.sendIcon}>
                    <Icon name='send' color={colors.white} size={20} />
                   
                </TouchableOpacity>
            </SafeAreaView>
            </View>
        )
    }
}

const style = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: colors.border,
    borderRadius: 100,
    elevation: 2,
    backgroundColor: colors.white,
    margin: 8
  },
  textInput: {
    flexGrow: 1,
    padding: 8,
    paddingLeft: 16,
    paddingRight: 16
  },
  sendIcon: {
    margin: 4,
    borderRadius: 100,
    backgroundColor: colors.primary,
    padding: 8
  }
})
