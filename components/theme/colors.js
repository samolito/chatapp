const colors = {
    //primary: '#26ADFF',
    //text: 'rgb(51,39,88)',
    //white: '#FFF',
    //border: '#CCCCCC',
    primary:'#2196F3',
    primary_dark:'#1976D2',
    primary_light:'#BBDEFB',
    accent:'#03A9F4',
    primary_text:'#212121',
    secondary_text:'#757575',
    white:'#FFFFFF',
    border:'#BDBDBD'
    }
 
  export default colors
  